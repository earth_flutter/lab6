import 'package:checkbox/checkboxTile_widget.dart';
import 'package:checkbox/checkbox_widget.dart';
import 'package:checkbox/dropdown_widget.dart';
import 'package:checkbox/radio_widget.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
      title: 'UI Extension',
      home: MyApp(),
      ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(title: Text('Ui Extension'),),
      body: ListView(children: [
        ListTile(
          title: Text('CheckBox'),
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => CheckBoxWidget()),);
          },
        ),
        ListTile(
          title: Text('CheckBoxTile'),
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => CheckBoxTileWidget()),);
          },
        ),
        ListTile(
          title: Text('DropDown'),
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => DropDownWidget()),);
          },
        ),
        ListTile(
          title: Text('Radio'),
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => RadioWidget()),);
          },
        ),
      ],),
      );
    
  }
}