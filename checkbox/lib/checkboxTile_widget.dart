import 'package:flutter/material.dart';

class CheckBoxTileWidget extends StatefulWidget {
  CheckBoxTileWidget({Key? key}) : super(key: key);

  @override
  _CheckBoxTileWidgetState createState() => _CheckBoxTileWidgetState();
}

class _CheckBoxTileWidgetState extends State<CheckBoxTileWidget> {
  bool check1 = false;
  bool check2 = false;
  bool check3 = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(appBar: AppBar(title: Text('ComboBoxTile'),),
      body: ListView(children: [
        CheckboxListTile(
          title: Text('check1'),
          subtitle: Text('Sub check 1'),
          value: check1, 
          onChanged: (value) {
            setState(() {
              check1 = value!;
            }
            );
        }
        ),
        CheckboxListTile(
          title: Text('check2'),
          subtitle: Text('Sub check 2'),
          value: check2, 
          onChanged: (value) {
            setState(() {
              check2 = value!;
            }
            );
        }
        ),
        CheckboxListTile(
          title: Text('check3'),
          subtitle: Text('Sub check 3'),
          value: check3, 
          onChanged: (value) {
            setState(() {
              check3 = value!;
            }
            );
        }
        ),
        TextButton.icon(onPressed: (){
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('check1: $check1,check2: $check2,check3: $check3'),));
        }, icon: Icon(Icons.save), label:Text('save'),
        )
      ],)
      ),
    );
  }
}